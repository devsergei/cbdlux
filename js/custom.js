$(window).load(function(){
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};
});
/* viewport width */
function viewport(){
	var e = window, 
		a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
/* viewport width */
$(function(){
	/* placeholder*/	   
	$('input, textarea').each(function(){
 		var placeholder = $(this).attr('placeholder');
 		$(this).focus(function(){ $(this).attr('placeholder', '');});
 		$(this).focusout(function(){			 
 			$(this).attr('placeholder', placeholder);  			
 		});
 	});
	/* placeholder*/

	// open/close menu
	$('.menu-btn').on('click', function(){
		$(this).toggleClass('active');
		$('.header-menu-wrapper').slideToggle();
		return false;
	});

	// open/close FAQ
	$('.faq-list__item').on('click', function(){
		$(this).find('p').slideToggle();
		$(this).siblings().find('p').slideUp()
		return false;
	});

	// open/close dropdown windows
	$('body').on('click', '.js-dd', function(){
		$('.dropdown-block').slideUp();
		$('.js-dd').not($(this)).parent().removeClass('active');
		if( $(this).parent().hasClass('active') ){
			$(this).parent().find('.dropdown-block').slideUp();
			$(this).parent().removeClass('active');
		} else{
			$(this).parent().find('.dropdown-block').slideDown();
			$(this).parent().addClass('active');
		}
		
		return false;
	});


	
	/* components */


	if($('.js-slider').length) {
		$('.js-slider').slick({
			infinite: true,
			speed: 300,
			slidesToShow: 4,
			slidesToScroll: 1,
			appendArrows: '.js-slider-btns',
			responsive: [
				{
				  breakpoint: 991,
				  settings: {
					slidesToShow: 3
				  }
				},
				{
				  breakpoint: 641,
				  settings: {
					slidesToShow: 2
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1
				  }
				}			
			]
		});
	};

	if($('.js-products-slider').length) {
		$('.js-products-slider').slick({
			infinite: true,
			speed: 300,
			dots: true,
			slidesToShow: 1,
			slidesToScroll: 1
		});
	};
	
	if($('.agriculture-image-wrapper').length) {
		$('.agriculture-image-wrapper').parallaxie({
			speed: 0.8
		});
	}
	

	/*
	
	if($('.styled').length) {
		$('.styled').styler();
	};
	if($('.fancybox').length) {
		$('.fancybox').fancybox({
			margon  : 10,
			padding  : 10
		});
	};
	
	if($('.scroll').length) {
		$(".scroll").mCustomScrollbar({
			axis:"x",
			theme:"dark-thin",
			autoExpandScrollbar:true,
			advanced:{autoExpandHorizontalScroll:true}
		});
	};
	
	*/
	
	/* components */
	
	

});

var handler = function(){
	
	var height_footer = $('footer').height();	
	var height_header = $('header').height();		
	//$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});
	
	
	var viewport_wid = viewport().width;
	var viewport_height = viewport().height;
	
	if (viewport_wid <= 991) {
		
	}
	
}
$(window).bind('load', handler);
$(window).bind('resize', handler);



